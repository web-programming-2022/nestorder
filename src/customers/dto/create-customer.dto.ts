import { IsNotEmpty, MaxLength, MinLength } from 'class-validator';

export class CreateCustomerDto {
  @MaxLength(500)
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  @MinLength(10)
  @MaxLength(10)
  tel: string;

  @MinLength(1)
  @MaxLength(1)
  @IsNotEmpty()
  gender: string;
}
