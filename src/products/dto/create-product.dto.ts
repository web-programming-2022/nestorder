import { IsNotEmpty, IsPositive, MaxLength } from 'class-validator';

export class CreateProductDto {
  @MaxLength(500)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNotEmpty()
  price: number;
}
